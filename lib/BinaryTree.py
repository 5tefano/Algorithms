"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class BinaryTree(object):
    """Implementation of Binary Trees"""

    def __init__(self, value):
        """Create a node"""
        self.left = None
        self.right = None
        self.parent = None
        self.value = value

    def Value(self):
        """Return the value of node"""
        return self.value

    def Left(self):
        """Return the left child of node"""
        return self.left

    def Right(self):
        """Return the right child of node"""
        return self.right

    def Parent(self):
        """Return the parent of node"""
        return self.parent

    def setParent(self, p):
        """Set the parent of node"""
        if p is not None:
            self.parent = p

    def insertLeft(self, t):
        """Insert the subtree t like left child of node"""
        if t.Parent() is None:
            t.setParent(self)
            self.left = t

    def deleteLeft(self):
        """Delete the left subtree of node"""
        self.left = None

    def insertRight(self, t):
        """Insert the subtree t like right child of node (self, this)"""
        if t.Parent() is None:
            t.setParent(self)
            self.right = t

    def deleteRight(self):
        """Delete the left subtree of node"""
        self.right = None

    @staticmethod
    def Depth(n):
        """Visit the tree in depth"""
        if n is not None:
            BinaryTree.Depth(n.Left())
            BinaryTree.Depth(n.Right())
            print (n.Value()),

    @staticmethod
    def findLeafs(root):
        """Find leafs of tree"""
        nodes = list()
        leaf = list()
        nodes.append(root)
        while nodes:
            tmp = nodes.pop(0)
            if not tmp.Left() and not tmp.Right():
                leaf.append(tmp)
            else:
                if tmp.Left():
                    nodes.append(tmp.Left())
                if tmp.Right():
                    nodes.append(tmp.Right())
        return leaf

    @staticmethod
    def subTree(v):
        """Create a simple tree with left and right child"""
        subRoot = BinaryTree(v)
        subRoot.insertLeft(BinaryTree(v + 1))
        subRoot.insertRight(BinaryTree(v + 2))
        return subRoot

    @staticmethod
    def createReverseBinaryTree(t):
        """Create reverse tree"""
        if t:
            t.reverseChild()
            BinaryTree.createReverseBinaryTree(t.Left())
            BinaryTree.createReverseBinaryTree(t.Right())

    def reverseChild(self):
        """Exchange the sons of node"""
        tmp = self.Left()
        self.left = self.Right()
        self.right = tmp
