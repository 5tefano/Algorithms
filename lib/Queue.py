"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Queue(object):

    def __init__(self):
        """Create a empty queue"""
        self.list = []

    def isEmpty(self):
        """Return true if is empty, else false"""
        return not self.list

    def enqueue(self, value):
        """Insert value in the queue"""
        self.list.append(value)

    def top(self):
        """Read the first element from queue"""
        return self.list[0]

    def dequeue(self):
        """Delete and return the first element of queue"""
        return self.list.pop(0)
