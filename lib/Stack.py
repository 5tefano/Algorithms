"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Stack(object):

    def __init__(self):
        """Create a empty stack"""
        self.list = []

    def isEmpty(self):
        """Return true if is empty, else false"""
        return not self.list

    def push(self, value):
        """Insert value in the stack"""
        self.list.insert(value, self.list[0])

    def top(self):
        """Read the first element from stack"""
        return self.list[0]

    def pop(self):
        """Delete and return the first element of stack"""
        tmp = self.top()
        del self.list[0]
        return tmp
