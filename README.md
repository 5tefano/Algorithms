# Algorithms

This project was created for learning to manage the principal data structures.

This project is licensed under the terms of the GNU GPL v3.

For license info, see the file "LICENSE".

Copyright (C) 2016 Stefano Giurgiano.
