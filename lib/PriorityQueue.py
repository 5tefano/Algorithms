"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from collections import namedtuple
class PriorityItem(namedtuple('PriorityItem', 'priority value pos')):
    pass

class PriorityQueue(object):

    def __init__(self, n):
        self.capability = n
        self.dim = 0
        self.H = []     # "Array" of PriorityItem

    def isEmpty(self):
        if self.dim > 0:
            return not self.H[0]

    def insert(self, x, p):
        if self.dim < self.capability:
            self.H.append(PriorityItem(priority=p, value=x, pos=self.dim))
            i = self.dim
            while i > 1 and self.H[i].priority < self.H[i / 2].priority:
                self.swap(self.H, i, i / 2)
                i = i / 2
            self.dim += 1
            return self.H[i]

    def all(self):
        for i in range(0, self.dim):
            print ("P: " + str(self.H[i].priority) + " -- V: " + str(self.H[i].value))

    def swap(self, h, i, j):
        tmp = h[i]
        h[i] = h[j]
        h[j] = tmp
        h[i].priority = i
        h[j].priority = j

    def min(self):
        return self.list[0]

    def decrease(self):
        return self.list.pop(0)

    def deleteMin(self):
        pass
