"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class List(object):
    """Implementation of Circulary Double Linked List with sentinel"""

    def __init__(self):
        """Create a element"""
        self.next = self
        self.prev = self
        self.value = None

    def isEmpty(self):
        """True if the list is empty, else false"""
        return self.next == self.prev == self

    def Head(self):
        """Return the head of list"""
        return self.next

    def Tail(self):
        """Return the tail of list"""
        return self.prev

    def Next(self, p):
        """Return the next element"""
        return p.next

    def Prev(self, p):
        """Return the previous element"""
        return p.prev

    def Finished(self, p):
        """Return TRUE if the list is finished"""
        return (self == p)

    def Read(self, p):
        """Read the value of element"""
        return p.value

    def Write(self, p, value):
        """Write the value of element"""
        p.value = value

    def Insert(self, p, value):
        """Insert the new element (tmp) before the element p"""
        tmp = List()
        tmp.value = value
        tmp.prev = p.prev
        p.prev.next = tmp
        tmp.next = p
        p.prev = tmp
        return tmp

    def Remove(self, p):
        """Remove the p element, and return the successive element"""
        p.prev.next = p.next
        p.next.prev = p.prev
        return p.next
