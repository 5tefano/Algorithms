"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Tree(object):
    """Implementation of Trees."""

    def __init__(self, value):
        """Create a node with value specificated"""
        self.parent = None
        self.sibling = None
        self.child = None
        self.value = value

    def read(self):
        """Read the value of node"""
        return self.value

    def leftMostChild(self):
        """Return the first child of node"""
        return self.child

    def rightSibling(self):
        """Return the sibling of node"""
        return self.sibling

    def Parent(self):
        """Return the parent of node"""
        return self.parent

    def setParent(self, p):
        """Set the parent of node"""
        if p is not None:
            self.parent = p

    def insertChild(self, t):
        """Insert the subtree t like left child of node"""
        if not t.Parent():
            t.setParent(self)
            t.sibling = self.child
            self.child = t

    def deleteChild(self):
        """Delete the first child of node and relative subtree"""
        delChild = self.leftMostChild()
        newChild = delChild.rightSibling()
        del(delChild)
        self.child = newChild

    def insertSibling(self, t):
        """Insert a sibling at node"""
        if t.Parent() is None:
            t.setParent(self)
            t.sibling = self.rightSibling()
            self.sibling = t

    def deleteSibling(self):
        """Delete the first sibling of node and relative subtree"""
        delSibling = self.rightSibling()
        newSibling = delSibling.rightSibling()
        self.sibling = newSibling

    @staticmethod
    def Depth(node):
        if node:
            print (node.read()),
            tmp = node.leftMostChild()
            while tmp:
                Tree.Depth(tmp)
                tmp = tmp.rightSibling()

    @staticmethod
    def findLeafs(root):
        """Find leafs of tree"""
        nodes = list()
        leaf = list()
        nodes.append(root)
        while nodes:
            tmp = nodes.pop(0)
            add = tmp.leftMostChild()
            if not add:
                leaf.append(tmp)
            else:
                while add:
                    nodes.append(add)
                    add = add.rightSibling()
        return leaf

    @staticmethod
    def subTree(v):
        """Create a simple tree with left and right child"""
        subRoot = Tree(v)
        subRoot.insertChild(Tree(v + 1))
        subRoot.leftMostChild().insertSibling(Tree(v + 3))
        subRoot.leftMostChild().insertSibling(Tree(v + 2))
        return subRoot

    """ The result of createStaticTree():
                                                    0
                         /                          |                            \
                        1                           2                             3
                /       |       \             /     |        \             /      |        \
               4        8       12          16      20        24         28       32        36
             / | \    / | \    / | \      / | \    / | \     / | \      / | \    / | \     / | \
            5  6  7  9 10 11  13 14 15  17 18 19  21 22 23  25 26 27  29 30 31  33 34 35  37 38 39
    """
    @staticmethod
    def createStaticTree():
        """Create a tree with 40 nodes"""
        i = 0
        root = Tree.subTree(i)
        i += 4
        leaf = Tree.findLeafs(root)
        while leaf:
            tmp = leaf.pop(0)
            T = Tree.subTree(i)
            i += 4
            tmp.insertChild(T)
            T = Tree.subTree(i)
            i += 4
            tmp.leftMostChild().insertSibling(T)
            T = Tree.subTree(i)
            i += 4
            tmp.leftMostChild().rightSibling().insertSibling(T)
        return root
