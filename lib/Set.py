"""
Algorithms
Copyright (C) 2016  Stefano Giurgiano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Set(object):
    """Implementation of Set with boolean array"""

    def __init__(self, n):
        """Create a empty set"""
        self.V = []
        self.dim = 0
        self.capacity = n
        for i in range(0, n):
            self.V += [False]

    def size(self):
        """Return the size of set"""
        return self.dim

    def capacity(self):
        """Return the capacity of set"""
        return self.capacity

    def contains(self, value):
        """Return true if value is in set, else false"""
        return self.V[value]

    def insert(self, value):
        """Insert the value in the set"""
        if not self.contains(value):
            self.dim += 1
            self.V[value] = True

    def remove(self, value):
        """Remove the value from set"""
        if self.contains(value):
            self.V[value] = False
            self.dim -= 1

    @staticmethod
    def union(A, B):
        C = Set(max(A.capacity, B.capacity))
        for i in range(0, A.capacity):
            if A.contains(i):
                C.insert(i)
        for i in range(0, B.capacity):
            if B.contains(i):
                C.insert(i)

    @staticmethod
    def intersection(A, B):
        m = min(A.capacity, B.capacity)
        C = Set(m)
        for i in range(0, m):
            if A.contains(i) and B.contains(i):
                C.insert(i)

    @staticmethod
    def difference(A, B):
        diff = A.capacity - B.capacity
        if diff >= 0:
            C = Set(A.capacity)
            for i in range(0, A.capacity):
                if A.contains(i) and (i > B.capacity or not B.contains(i)):
                    C.insert(i)
